package pl.sdacademy.intents02b;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private final static String KEY_DATA = "pl.sdacademy.intents.KEY_DATA";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        String incomingText = getIntent().getStringExtra(KEY_DATA);

        if (!TextUtils.isEmpty(incomingText)) {
            ((TextView) findViewById(R.id.text)).setText(incomingText);
        }
    }
}
