package pl.sdacademy.multicomponentapplications;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MainActivity extends AppCompatActivity {

    private final static String COORDINATES_PATTERN = "^(\\-?\\d+(\\.\\d+)?),\\s*(\\-?\\d+(\\.\\d+)?)$";

    private final static String URI_COORDINATES = "geo:0,0?q={latitude},{longitude}";
    private final static String URI_ADDRESS = "geo:0,0?q={address}";

    private EditText editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editText = (EditText) findViewById(R.id.location_edit);

        findViewById(R.id.show).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showMap();
            }
        });
    }

    private void showMap() {
        String locationText = editText.getText().toString();

        Uri locationUri = getLocationUri(locationText);

        Intent intent = new Intent(Intent.ACTION_VIEW, locationUri);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }

    private Uri getLocationUri(String text) {
        if (isCoordinates(text)) {
            String[] coordinates = text.split(",");
            return Uri.parse(URI_COORDINATES.replace("{latitude}", coordinates[0]).replace("{longitude}", coordinates[1]));
        }

        return Uri.parse(URI_ADDRESS.replace("{address}", text));
    }

    private boolean isCoordinates(String text) {
        Pattern pattern = Pattern.compile(COORDINATES_PATTERN);
        Matcher matcher = pattern.matcher(text);
        return matcher.matches();
    }
}
