package pl.sdacademy.intents;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private final static String EXAMPLE_ACTION = "pl.sdacademy.intents.EXAMPLE_ACTION";
    private final static String KEY_DATA = "pl.sdacademy.intents.KEY_DATA";
    private EditText editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editText = (EditText) findViewById(R.id.edit_text);

        findViewById(R.id.show).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startImplicitIntent();
            }
        });
    }

    private void startImplicitIntent() {
        String text = editText.getText().toString();

        Intent implicitIntent = new Intent(EXAMPLE_ACTION);
        implicitIntent.putExtra(KEY_DATA, text);

        if (implicitIntent.resolveActivity(getPackageManager()) != null) {
            startActivity(implicitIntent);
        } else {
            Toast.makeText(this, R.string.resolve_error, Toast.LENGTH_SHORT).show();
        }
    }
}
