package pl.sdacademy.intents03;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private final static int REQUEST_CODE = 123;

    public final static String KEY_FIRST_NUMBER = "KEY_FIRST_NUMBER";
    public final static String KEY_SECOND_NUMBER = "KEY_SECOND_NUMBER";
    public final static String KEY_OPERATION = "KEY_OPERATION";

    public final static String KEY_RESULT = "KEY_RESULT";

    private EditText firstNumber;
    private EditText secondNumber;
    private TextView resultView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        firstNumber = (EditText) findViewById(R.id.number_first);
        secondNumber = (EditText) findViewById(R.id.number_second);
        resultView = (TextView) findViewById(R.id.result);

        findViewById(R.id.button_add).setOnClickListener(this);
        findViewById(R.id.button_subtract).setOnClickListener(this);
        findViewById(R.id.button_multiply).setOnClickListener(this);
        findViewById(R.id.button_divide).setOnClickListener(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE && resultCode == RESULT_OK) {
            String result = data.getStringExtra(KEY_RESULT);
            resultView.setText(result);
            return;
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(this, DataProcessingActivity.class);

        String tag = v.getTag().toString();
        intent.putExtra(KEY_OPERATION, tag);

        intent.putExtra(KEY_FIRST_NUMBER, firstNumber.getText().toString());
        intent.putExtra(KEY_SECOND_NUMBER, secondNumber.getText().toString());

        startActivityForResult(intent, REQUEST_CODE);
    }
}
