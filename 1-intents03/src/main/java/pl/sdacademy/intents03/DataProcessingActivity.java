package pl.sdacademy.intents03;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class DataProcessingActivity extends AppCompatActivity {

    public static final String OPERATION_ADD = "add";
    public static final String OPERATION_SUBTRACT = "subtract";
    public static final String OPERATION_MULTIPLY = "multiply";
    public static final String OPERATION_DIVIDE = "divide";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_processing);

        Bundle extras = getIntent().getExtras();

        final String firstNumber = extras.getString(MainActivity.KEY_FIRST_NUMBER);
        final String secondNumber = extras.getString(MainActivity.KEY_SECOND_NUMBER);
        final String operation = extras.getString(MainActivity.KEY_OPERATION);

        ((TextView) findViewById(R.id.first_number)).setText(String.valueOf(firstNumber));
        ((TextView) findViewById(R.id.second_number)).setText(String.valueOf(secondNumber));
        ((TextView) findViewById(R.id.operation)).setText(operation);

        findViewById(R.id.send_result).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double result = calculateResult(operation, firstNumber, secondNumber);
                returnResult(result);
            }
        });
    }

    private double calculateResult(String operation, String firstNumberString, String secondNumberString) {

        double firstNumber = Double.parseDouble(firstNumberString);
        double secondNumber = Double.parseDouble(secondNumberString);

        switch (operation) {
            case OPERATION_ADD:
                return firstNumber + secondNumber;
            case OPERATION_SUBTRACT:
                return firstNumber - secondNumber;
            case OPERATION_MULTIPLY:
                return firstNumber * secondNumber;
            case OPERATION_DIVIDE:
                return firstNumber / secondNumber;
        }

        throw new UnsupportedOperationException("Invalid operation");
    }

    private void returnResult(double result) {
        Intent resultIntent = new Intent();
        resultIntent.putExtra(MainActivity.KEY_RESULT, String.valueOf(result));
        setResult(RESULT_OK, resultIntent);
        finish();
    }
}
